#!/usr/bin/env python

from scipy.io.wavfile import write
import matplotlib.pyplot as plt
import numpy as np
import librosa.display


# song importation;  y: audio time series (int array), sr: sampling rate
y, sr = librosa.load("02_Ghost_Pong.wav")                                        # change the name to load another file

# -------------------- creation of a sample -------------------- #
song_duration = librosa.get_duration(y=y, sr=sr)                                 # duration of song (sec)
sample_start = 0.384                                                             # start of the sample (sec) here
sample_end = 3.051                                                               # end of the sample (sec) here:
sample_duration = sample_end - sample_start                                      # sample duration in seconds
position_start = int((sample_start * len(y)) / song_duration)                    # starting position of sample
position_end = position_start + int((sample_duration * len(y)) / song_duration)  # end of the sample
sample = y[position_start:position_end]                                          # the sample (fraction of y)
# ---------------------------------------------------------------- #
# ---------------------------------------------------------------- #

# ---------------------- Calculation of FFT ---------------------- #
# Full song
freq_song = np.fft.fftfreq(y.size, 1/sr)                                         # frequencies in the song
fft_song = np.fft.fft(y)                                                         # FFT of the sound
amplitude_song = np.abs(fft_song)                                                # amplitude is just absolute of FFT

# Sample
freq_sample = np.fft.fftfreq(sample.size, 1/sr)                                  # frequencies in the sample
fft_sample = np.fft.fft(sample)                                                  # FFT of the sample
amplitude_sample = np.abs(fft_sample)                                            # amplitude if just absolute of FFT
# --------------------------------------------------------------- #
# --------------------------------------------------------------- #

# ----------------------- Filtering sound ----------------------- #

lsa = len(sample)
cross = []
fft_filtered = fft_song
step = lsa
for i in range(0, len(y) - lsa, step):
    test = y[i:i+lsa]
    fft_test = np.fft.fft(test)
    cross.append(np.corrcoef(amplitude_sample, np.abs(fft_test))[0][1])

for i in range(len(cross)-1):
    if cross[i] > np.max(cross)/2# and len(fft_song[i:i+lsa]) == len(fft_sample):
        # print(i, ")", len(fft_filtered[i:i+lsa]), len(fft_song[i:i+lsa]), len(fft_sample))
        temp = y[i*step:i*step+lsa]
        temp_fft = np.fft.fft(temp)
        temp_fft = temp_fft - fft_sample
        temp_inv = np.fft.irfft(temp_fft)
        y[i*step:i*step+lsa] = temp_inv

print("done")
y_filtered = y

song_duration = y.size/sr                                                              # duration of the song (in sec)
song_time = np.linspace(0, song_duration, y.size)                                      # the discretized time
fft_song_cropped = fft_song[0:len(song_time)]


# Export the file:
write('test_filtered.wav', sr, y_filtered)  # write the filtered song
# --------------------------------------------------------------- #
# --------------------------------------------------------------- #

# ---------------------------- PLOTS ---------------------------- #
# Full song #
"""
plt.figure(1)
plt.plot()
plt.title("Full song amplitude of signal NOT normalized (half of FFT)")
plt.plot(freq_song[1:int(len(fft_song)/2)],
         amplitude_song[1:int(len(fft_song)/2)])
"""

# uncomment this to plot the sound as it will be displayed in a software like Audacity
"""
plt.figure(2)
plt.plot()
plt.title("Full song audio signal per seconds")
song_duration = y.size/sr                                                        # duration of the song (in sec)
song_time = np.linspace(0, song_duration, y.size)                                # the discretized time
plt.plot(song_time, y)
"""

# Sample #
"""
plt.figure(3)
plt.plot()
plt.title("Sample amplitude of signal NOT normalized (half of FFT)")
plt.plot(freq_sample[1:int(len(fft_sample)/2)],
         amplitude_sample[1:int(len(fft_sample)/2)])
"""

# uncomment this to plot the sample as it will be displayed in a software like Audacity
"""
plt.figure(4)
plt.plot()
plt.title("Sample song audio signal per seconds")
sample_duration = sample.size/sr                                                        # duration of the song (in sec)
sample_time = np.linspace(0, sample_duration, sample.size)                              # the discretized time
plt.plot(sample_time, sample)
"""

# correlation coefficient (function of time)
"""
plt.figure(5)
lsa = len(sample)                                                                       # lsa = 58_807
cross = []
plt.title("Correlation coefficient with a step of %d" % (lsa//10))
for i in range(0, len(y) - lsa, lsa//10):                                               # len(y) - lsa = 6_372_737
    test = y[i:i+lsa]
    fft_test = np.fft.fft(test)
    cross.append(np.corrcoef(amplitude_sample, np.abs(fft_test))[0][1])
plt.plot(cross)
"""

"""
plt.figure(6)
plt.subplot(211)
plt.title("Correlation coefficient with a step of %d" % (lsa//10))
plt.plot(cross, linewidth=0.5)

plt.subplot(212)
plt.title("Full song audio signal per seconds")
plt.plot()
plt.plot(song_time, y_filtered[0:len(song_time)], linewidth=0.2)                       # y_filtered and not y

plt.tight_layout()
plt.show()
"""
