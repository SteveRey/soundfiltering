# TODO: extract sample --> specify length before doing this
# TODO: compose same sample with Python --> save in mp3 file
# TODO: listen to the file

import matplotlib.pyplot as plt
from scipy.io.wavfile import write
import numpy as np
import librosa.display

# song importation
y, sr = librosa.load("02_Ghost_Pong.wav")  # y: audio time series, sr: sampling rate

# creation of a sample
# TODO: idea --> declare sample array as same size of initial array, but filled with 0, then, copy the value of sample
song_duration = librosa.get_duration(y=y, sr=sr)  # duration of song in seconds
sample_start = 0.384  # start of the sample (in seconds) here: 0.384
sample_end = 3.051  # end   of the sample (in seconds) here: 3.051
sample_duration = sample_end - sample_start  # sample duration in seconds
position_start = int((sample_start * len(y)) / song_duration)  # the starting position of the sample
position_end = position_start + int((sample_duration * len(y)) / song_duration)  # the end of the sample
sample = y[position_start:position_end]  # the sample (fraction of y)
scaled = np.int16(sample / np.max(np.abs(sample)) * sr)  # need to scale to have integer and not float

stft_y = np.abs(librosa.stft(y))
stft_sample = np.abs(librosa.stft(sample))
fft_y = np.fft.fft(y)            # 1D discrete Fourier Transform of entire song
# fft_sample = np.fft.fft(sample)  # 1D discrete Fourier Transform of the sample
fft_sample = librosa.core.fft_frequencies()
n_y = y.size
n_sample = sample.size
time_step = 1 / sr
freq_y = np.fft.fftfreq(n_y, time_step)            # Discrete Fourier Transform frequencies of entire song
freq_sample = np.fft.fftfreq(n_sample, time_step)  # Discrete Fourier Transform frequencies of sample

print("y length: {0}, sample length: {1} fft_y length: {2}, fft_sample length {3}"
      .format(len(y), len(sample), len(fft_y), len(fft_sample)))
# returns: y length: 6431544, sample length: 58807 fft_y length: 6431544, fft_sample length 58807
print()
print("y: ")
print(y)
print("---------------------------------")
print("fft_y: ")
print(fft_y)
print()
print("fft_sample: ")
print(fft_sample)
print()
print("---------------------------------")
print("---------------------------------")
print("freq_y length: {0}, freq_sample length: {1}".format(len(freq_y), len(freq_sample)))
# returns: freq_y length: 6431544, freq_sample length: 58807
print("freq_y: ")
print(freq_y)
print()
print("freq_sample: ")
print(freq_sample)
print()

librosa.display.specshow(librosa.amplitude_to_db(stft_sample, ref=np.max), y_axis='log', x_axis='time')
plt.title('STFT Power Spectrogram')
plt.colorbar(format='%+2.0f dB')
plt.tight_layout()
plt.show()


"""
plt.plot(freq_y, fft_y.real)
plt.show()
"""


# To export the file: scipy.io.wavfile.write(filename, sample rate, data)
# write('test.wav', sr, y_filtered)  # write the 2nd half of the song

